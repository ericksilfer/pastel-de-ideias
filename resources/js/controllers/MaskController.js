export class MaskController {

    constructor() {

        this._settings = {
            target: 'data-mask'
        };

        this.init();
    }

    init() {
        this.addListeners();
        this.applyMasks();
    }

    addListeners() {
        document.body.addEventListener('keyup', e => this.validateElement(e));
    }

    validateElement(e) {
        if (!e.target.hasAttribute(this._settings.target)) 
            return;

        this.applyMask(e.target);
    }

    get items() {
        return document.querySelectorAll('[' + this._settings.target + ']');
    }

    applyMasks() {
        for (let item of this.items) {
            this.applyMask(item);
        }
    }

    applyMask(target) {
        let value = target.value;
        let type = target.getAttribute(this._settings.target);

        switch (type) {

            case 'money':

                value = value.replace(/\./g, '');
                value = value.replace(/\,/, '.');
                console.log(value)

                value = parseFloat(value).formatMoney(2, ',', '.');

                if (isNaN(parseFloat(value)))
                    value = null;

                break;
        }

        target.value = value;
    }
}

Number.prototype.formatMoney = function(c, d, t){
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
