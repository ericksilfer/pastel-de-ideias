export class FormController {

    constructor() {
        
        this.cleanButton = document.querySelector('#clean');

        if (this.cleanButton) {
            this.setCleanButton();
        }
    }

    setCleanButton() {
        
        this.cleanButton.addEventListener("click", function() {

            let form = this;

            while(form.tagName != 'FORM') {
                form = form.parentNode;

                if (form.tagName == 'BODY')
                    break
            }

            form.reset();
        });
    }
}