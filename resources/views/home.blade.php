@extends('layouts.default')
 
@section('title')
Home
@stop

@section('content')

@if ($products->count() > 0)
    @foreach($products as $product)
        @component('components.order', ['product' => $product])@endcomponent
    @endforeach
@else
    <p class="text-center">Não há produtos cadastrados!</p>
@endif
@stop