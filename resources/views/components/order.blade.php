<div class="card with-img">

    <img src="{{ asset('storage/'.$product->filepath) }}" class="img-fluid position-left" alt="">

    <div class="card-header b-red p-left">
        <div class="row">
            <div class="col-md-9">
                <h5 class="red">"{{ $product->title }}"</h5>
            </div>
            <div class="col-md-3 text-right">
                <span>{{ App\Helpers\DefaultHelper::inputValue($product, 'price', [2, ',', '.'], 'money') }}</span>
            </div>
        </div>
    </div>

    <div class="card-body">
        <p>
            <strong>Sabor:</strong>: {{ $product->flavor }}
        </p>
        <p>
            <strong>Descrição:</strong>: {{ $product->description }}
        </p>
    </div>
</div>