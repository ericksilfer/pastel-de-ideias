@extends('layouts.default')
 
@section('title')
Lista de Produtos.
 @stop

 
@section('content')
<div class="card">
        
	<div class="card-header p-left b-yellow">
		<h5 class="font-red yellow">Lista de Produtos</h5>
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Título</th>
					<th scope="col">Sabor</th>
					<th scope="col">Tipo</th>
					<th scope="col">Ação</th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
						<tr>
							<th scope="row">{{ $product->id }}</th>
							<td>{{ $product->title }}</td>
							<td>{{ $product->flavor }}</td>
							<td>{{ App\Helpers\DefaultHelper::getProductType($product->type) }}</td>
							<td>
								<a href="{{ URL::action('ProductController@edit', ['id' => $product->id]) }}"> Editar</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $products->links() }}
		</div>
	</div>
</div>
@stop