@extends('layouts.default')
 
@section('title')
Cadastro de Produto.
@stop

@section('content')

@component('components.messages')@endcomponent

<div class="card" style="margin-botton: 40px">
    
    <form action="{{ URL::action('ProductController@store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        @if (!empty(App\Helpers\DefaultHelper::inputValue($product, 'id')))
            <input type="hidden" name="id" value="{{ App\Helpers\DefaultHelper::inputValue($product, 'id') }}">
        @endif

        <div class="card-header p-left b-yellow">
            <div class="row">
                <div class="col-md-9">
                    <h5 class="font-red yellow">Monte aqui o seu cardápio. O que está esperando?</h5>
                </div>
                <div class="col-md-3">
                    <div class="custom-control custom-switch">
                        <label class="font-red" style="width: 100px;">Comida</label>
                        <input type="checkbox" class="custom-control-input" id="switch1" name="type" value="drink" @if($product->type == 'drink') checked @endif>
                        <label class="font-red custom-control-label" for="switch1">Bebida</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-5">
                    <input type="text" name="title" class="form-control" placeholder="Título do Pedido" value="{{ App\Helpers\DefaultHelper::inputValue($product, 'title') }}">
                </div>

                <div class="form-group col-md-5">
                    <input type="text" name="flavor" class="form-control" placeholder="Sabor" value="{{ App\Helpers\DefaultHelper::inputValue($product, 'flavor') }}">
                </div>

                <div class="form-group col-md-2">
                    <input type="text" name="price" data-mask="money" class="form-control" placeholder="R$" value="{{ App\Helpers\DefaultHelper::inputValue($product, 'price', [2, ',', '.'], 'money') }}">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col">
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3">{{ App\Helpers\DefaultHelper::inputValue($product, 'description') }}</textarea>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col">
                    <div class="box">
                        <input type="file" name="file" id="file-5" class="inputfile inputfile-4" data-multiple-caption="{count} files selected" multiple="">
                        <label for="file-5">
                            <figure>
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                                </svg>
                            </figure> 
                            <span>Jogue aqui o arquivo de imagem do seu pastel ou clique para localizar a pasta.</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="buttons">
                <button type="button" class="btn btn-primary red" id="clean">LIMPAR</button>
                <button type="submit" class="btn btn-primary yellow">CADASTRAR</button>
            </div>
        </div>
    </form>
</div>


@if (!empty(App\Helpers\DefaultHelper::inputValue($product, 'id')))
    <div>
        <p class="preview text-center">
            Veja como será apresentado ao cliente
        </p>    
    </div>

    @component('components.order', ['product' => $product])@endcomponent
@endif

@stop