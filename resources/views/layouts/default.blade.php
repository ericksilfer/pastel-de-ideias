<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    @yield('style')

    <title>@yield('title') | Pastel de Ideias</title>
  </head>
  <body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Pastel</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="{{ URL::action('HomeController@index') }}">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Produtos
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="{{ URL::action('ProductController@index') }}">Lista</a>
					<a class="dropdown-item" href="{{ URL::action('ProductController@create') }}">Novo</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<header class="text-center py-md-3">
			<img src="{{ asset('/media/Logo.svg') }}"  class="img-fluid logo" alt="">
			<div class="behind" >
				<img src="{{ asset('/media/pasteis-img.png') }}" alt="">
			</div>
			<div class="on" >
				<img src="{{ asset('/media/pastel-paralax@2x.png') }}" alt="">
			</div>
		</header>

		<main class="py-md-3 pl-md-5 bd-content" role="main">
			<div id="app">

				@yield('content')
			</div>
		</main>
	</div>
    
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	
    @yield('script')

	<script src="{{ mix('/js/app.js') }}"></script>
  </body>
</html>