<?php 

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Helpers\UploadHelper;

class ProductsRepository
{
	private $product;

	public function __construct(Product $product)
	{
		$this->product = $product;
	}

	public function getList()
	{
		return $this->product->query()
							->isActive()
							->paginate(5);
	}

	public function saveData(Request $request)
	{
			return \DB::transaction(function () use ($request) {

				if ($request->hasFile('file')) {
					UploadHelper::setPath('products')
								->setDisk('public')
								->setFile($request->file)
								->save();
				}
				
				return $this->product->saveData($request, UploadHelper::getFullPath());
			});
			try {
		} catch (Exception $e) {
			return FALSE;
		}
	}
}