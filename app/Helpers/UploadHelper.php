<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Ocr\Image;

class UploadHelper
{
    private static $disk = 'local';
    private static $path;
    private static $newPath = '';
    private static $name;
    private static $resizes;
    private static $file;
    private static $extension;
    private static $types = ['png', 'jpg', 'jpeg'];
    private static $currentDate;
    private static $instance;

    private static function getInstance(): UploadHelper
    {
        if (empty(self::$stance)) {
            self::setInstance();
        }

        return self::$instance;
    }

    public static function render()
    {
        $filepath = Storage::disk(self::$disk)->path(self::$path);

        if (!File::exists($filepath)) {
            abort(404);
        }

        $file = File::get($filepath);
        $type = File::mimeType($filepath);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public static function getFullPath(): string
    {
        return self::$newPath;
    }

    public static function getInfoFile(): array
    {
        return pathinfo(self::$newPath);
    }

    public static function getInformation()
    {
        return pathinfo(self::$path);
    }

    public static function getName()
    {
        $fileNames = explode('.', self::$name);

        $fileNames[0] = $fileNames[0]. '-' .self::$currentDate;

        return implode('.', $fileNames);
    }

    private static function setInstance()
    {
        self::$instance = new static();
    }

    public static function setDisk($disk)
    {
        self::$disk = $disk;
        return self::getInstance();
    }

    public static function setPath($path)
    {
        self::$path = $path;
        return self::getInstance();
    }

    public static function getPath()
    {
        return Storage::disk(self::$disk)->path(self::$path);
    }

    public static function setFile($file)
    {
        self::$file = $file;
        self::$name = $file->getClientOriginalName();
        self::$extension = $file->getClientOriginalExtension();
        self::$currentDate = date('Y-m-d-H-i');

        return self::getInstance();
    }

    public static function save()
    {
        if (in_array(self::$file->getClientOriginalExtension(), self::$types)) {
            self::insertFile();
        }
    }

    public static function remove()
    {
        Storage::disk(self::$disk)->delete(self::$path);
    }

    private static function insertFile()
    {
        self::$newPath = self::$file->store(self::$path, self::$disk);

        $information = self::getInfoFile();
    }
}
