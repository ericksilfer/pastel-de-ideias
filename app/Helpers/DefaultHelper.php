<?php

namespace App\Helpers;

class DefaultHelper
{
    public static function formatToFloat($number)
    {
        return preg_replace('/\,/', '.', preg_replace('/\./', '', $number));
    }

    public static function getProductType(string $type): string
    {
        if ($type == 'drink') {
            return 'Bebida';
        }

        return 'Comida';
    }

    public static function inputValue($model, $field, $format = null, $type = 'date')
    {
        if (isset($model) && is_null($format)) {
            return $model->{$field};
        } elseif (!is_null($format) && !empty($model->{$field}) && $type == 'date') {
            return $model->{$field}->format($format);
        } elseif (!is_null($format) && !empty($model->{$field}) && $type == 'money') {
            return number_format((float)$model->{$field}, $format[0], $format[1], $format[2]);
        }
        return old($field);
    }
}