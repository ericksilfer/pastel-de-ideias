<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Helpers\DefaultHelper;

class Product extends Model
{
    use SoftDeletes;
    
	const TYPE_FOOD = 'food'; 
	const TYPE_DRINK = 'drink';

    public function scopeIsActive($query)
    {
    	return $query->where('active', true);
    }

    public function saveData(Request $request, $filepath = null): Product
    {
    	if ($request->has('id')) {
    		$product = Product::findOrFail($request->id);
    	} else {
    		$product = new Product;
		}
		
		if (!empty($filepath)) {
			$product->filepath = $filepath;
		}

		$product->title = $request->title;
		$product->description = $request->description;
		$product->flavor = $request->flavor;
		$product->type = $request->type ?? 'food';
		$product->price = DefaultHelper::formatToFloat($request->price);
		$product->save();

		return $product;
    }
}
