<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Repositories\ProductsRepository;

class ProductController extends Controller
{
    private $products;

    public function __construct(ProductsRepository $products)
    {
        $this->products = $products;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product.index')->with(['products' => $this->products->getList()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.edit')->with(['product' => new Product()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $productInserted = $this->products->saveData($request);

        if (FALSE == $productInserted) {
            return back()->withInput()->withErrors(['error' => 'Não foi possível cadastrar o produto!']);
        }

        return redirect()->action('ProductController@edit', ['id' => $productInserted->id])
                            ->with(['message' => 'Produto cadastrado com sucesso!']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('product.edit')->with(['product' => $product]);
    }

    public function show(Product $product)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();

            return redirect()->action('ProductController@index')
                            ->with(['message' => 'Produto removido com sucesso!']);
        } catch (\Exception $e) {
            return back()->withErrors(['Não foi possível remover o produto!']);
        }
    }
}
