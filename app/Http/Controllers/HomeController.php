<?php

namespace App\Http\Controllers;

use App\Repositories\ProductsRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $products;

    public function __construct(ProductsRepository $products)
    {
        $this->products = $products;
    }

    public function index()
    {
        return view('home')->with(['products' => $this->products->getList()]);
    }
}
