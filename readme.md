## Pastel de Ideias

Esse projeto utiliza o framework laravel5.7.28.
Para iniciar o projeto é necessário seguir os passos:

## Passo 1:

git clone git@gitlab.com:ericksilfer/pastel-de-ideias.git

## Passo 2

cd pastel-de-ideias

cp .env.example .env

- O arquivo .env recebe as informações de conexão com o banco de dados, você deve inserir o nome do usuário, senha e database nas seguinte linhas: 
- DB_DATABASE=SEU_DATABASE
- DB_USERNAME=SEU_USUÀRIO
- DB_PASSWORD=SUA_SENHA

## Passo 3

composer install

## Passo 4

php artisan migrate

## Passo 5

php artisan storage:link

## Passo 5

php artisan serve

- O comando serve disponibiliza um servidor de desenvolvimento, assim basta inserir no navegador a seguinte url: http://127.0.0.1:8000